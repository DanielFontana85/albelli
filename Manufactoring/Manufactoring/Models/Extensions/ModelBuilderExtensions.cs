﻿using Manufactoring.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Manufactoring.Helpers
{
    public static class ModelBuilderExtensions
    {
        public static void OrderModelBuilder(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Order>()
                .Property(p => p.OrderId)
                .ValueGeneratedNever();
            modelBuilder.Entity<Order>()
                .HasMany(o => o.Products);
            modelBuilder.Entity<Order>()
                .Ignore(o => o.RequiredBinWidth);
        }

        public static void ProductModelBuilder(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Product>()
                .HasOne(p => p.Type); 
            modelBuilder.Entity<Product>()
                .Property(p => p.Quantity)
                .IsRequired();
        }

        public static void ProductTypeModelBuilder(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ProductType>()
                .Property(p => p.Id)
                .ValueGeneratedNever();
            modelBuilder.Entity<ProductType>()
                .Property(p => p.Name)
                .HasMaxLength(50)
                .IsRequired();
            modelBuilder.Entity<ProductType>()
                .Property(p => p.Width)
                .IsRequired();
            modelBuilder.Entity<ProductType>()
                .Property(p => p.Width)
                .HasColumnType("decimal(18,2)");
        }
    }
}
