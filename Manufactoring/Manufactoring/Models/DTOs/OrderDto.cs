﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Manufactoring.DTOs
{
    public class OrderDto
    {
        public int OrderID { get; set; }
        public IEnumerable<ProductDto> Products { get; set; }

        public bool IsValid(ref string msg)
        {
            if (OrderID <= 0)
            {
                msg = $"Order id is lower than 0.";
                return false;
            }

            if (Products == null)
            {
                msg = $"Products are mandatory.";
                return false;
            }

            if (Products.Any(p => p.Quantity <= 0))
            {
                msg = $"One or more product's quantities are lower than 0.";
                return false;
            }

            return true;
        }
    }

    public class ProductDto
    {
        public string ProductType { get; set; }
        public int Quantity { get; set; }
    }

    public class OutputOrderDto : OrderDto
    {
        public int RequiredBinWidth { get; set; }
    }
}
