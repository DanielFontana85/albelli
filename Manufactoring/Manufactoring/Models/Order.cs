﻿using System.Collections.Generic;
using System.Linq;

namespace Manufactoring.Models
{
    public class Order
    {
        public int OrderId { get; set; }
        public IEnumerable<Product> Products { get; set; }

        public decimal RequiredBinWidth { get { return OrderCalculator.CalculateBinWidth(this.Products); } }
    }

    public static class OrderCalculator
    {
        public static decimal CalculateBinWidth(IEnumerable<Product> products)
        {
            decimal _requiredBinWidth = 0;
            if (products != null && products.Any())
            {
                foreach (var product in products)
                {
                    var quantityPerStack = product.Quantity / (product.Type.NbrOfItemsCanStack > 0 ? product.Type.NbrOfItemsCanStack : 1);
                    quantityPerStack = product.Quantity % product.Type.NbrOfItemsCanStack > 0 ? quantityPerStack + 1 : quantityPerStack;
                    _requiredBinWidth += product.Type.Width * quantityPerStack;
                }
            }

            return _requiredBinWidth;
        }
    }
}
