﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Manufactoring.Models
{
    public class ProductType
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public decimal Width { get; set; }
        public int NbrOfItemsCanStack { get; set; }
    }
}
