using Manufactoring.Data;
using Manufactoring.Data.Repository;
using Manufactoring.Models;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace Manufactoring
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<DataContext>(option =>
            {
                option.UseSqlServer(Configuration.GetConnectionString("DefaultConnection"));
            });

            services.AddControllers();

            services.CorsConfiguration();
            services.SwaggerConfiguration();
            services.AutoMapperConfiguration();

            services.AddScoped<IDataContext, DataContext>();
            services.AddTransient<IReadRepository<ProductType>, ProductTypeRepository>();
            services.AddTransient<IReadRepository<Order>, OrderRepository>();
            services.AddTransient<IWriteRepository<Order>, OrderRepository>();
            services.AddTransient<IProductTypeService, ProductTypeService>();
            services.AddTransient<IOrderService, OrderService>();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseCors(ConfigurationServicesExtensions.CORS_POLICY);
            }

            app.UseHttpsRedirection();
            app.UseRouting();
            app.UseAuthorization();

            app.UseStaticFiles();
            app.UseSwaggerMiddleware();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
