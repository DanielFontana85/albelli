﻿using AutoMapper;
using Manufactoring.DTOs;
using Manufactoring.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Manufactoring
{
    public class AutoMapperProfiles : Profile
    {
        public AutoMapperProfiles()
        {
            CreateMap<OrderDto, Order>()
                .ForMember(obj => obj.Products, opt => opt.MapFrom(dto =>
                    dto.Products != null ? dto.Products.GroupBy(p => p.ProductType).Select(g => new Product()
                    {
                        Quantity = g.Sum(x => x.Quantity),
                        Type = new ProductType() { Name = g.Key }
                    }) : null
                 ));

            CreateMap<Order, OutputOrderDto>()
                .ForMember(dto => dto.Products, opt => opt.MapFrom(obj =>
                    obj.Products != null ? obj.Products.Select(x => new ProductDto()
                    {
                        ProductType = x.Type.Name,
                        Quantity = x.Quantity
                    }) : null
                 ));
        }
    }
}
