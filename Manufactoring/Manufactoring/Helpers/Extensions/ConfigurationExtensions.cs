﻿using Microsoft.AspNetCore.Builder;
using Swashbuckle.AspNetCore.SwaggerUI;

namespace Manufactoring
{
    public static class ConfigurationExtensions
    {
        

        #region Configure

        public static void UseSwaggerMiddleware(this IApplicationBuilder app)
        {
            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Manufactoring API V1");
                c.InjectStylesheet("/swagger-ui/swagger-ui.css");
                c.InjectJavascript("/swagger-ui/swagger-ui.js");
                c.DefaultModelsExpandDepth(-1);
                c.DocExpansion(DocExpansion.List);
            });
        }

        #endregion
    }
}
