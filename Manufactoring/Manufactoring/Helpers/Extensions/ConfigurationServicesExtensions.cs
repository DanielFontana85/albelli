﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Manufactoring
{
    public static class ConfigurationServicesExtensions
    {
        #region Configure Services

        public const string CORS_POLICY = "CorsPolicy";

        public static void CorsConfiguration(this IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddPolicy(CORS_POLICY,
                    builder => builder.AllowAnyOrigin()
                    .AllowAnyMethod()
                    .AllowAnyHeader());
            });
        }
        public static void SwaggerConfiguration(this IServiceCollection services)
        {
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo
                {
                    Version = "v1",
                    Title = "Manufactoring API",
                    Description = "These are ASP.NET Core Web API endpoints for Manufactoring",
                    Contact = new OpenApiContact
                    {
                        Name = "Daniel Fontana",
                        Email = "dfontana85@gmail.com",
                    },
                });
            });
        }
        public static void AutoMapperConfiguration(this IServiceCollection services)
        {
            services.AddAutoMapper(c => c.AddProfile<AutoMapperProfiles>(), typeof(Startup));
        }

        #endregion
    }
}
