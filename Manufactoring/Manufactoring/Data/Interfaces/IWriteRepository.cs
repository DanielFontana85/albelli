﻿using System.Threading.Tasks;

namespace Manufactoring.Data.Repository
{
    public interface IWriteRepository<T> where T : class
    {
        Task Add(T obj );
    }
}
