﻿using Manufactoring.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Manufactoring.Data
{
    public class Seed
    {
        public static void SeedData(IServiceProvider scope)
        {
            // Get an instance of the DbContext from the DI container
            var context = scope.GetRequiredService<IDataContext>();
            var dbChanges = false;

            //Create Db if it doesn't exist
            context.Database.Migrate();

            //Create ProductTypes
            SeedProductTypes(ref context, ref dbChanges);

            if (dbChanges)
                context.SaveChanges();
        }

        public static void SeedProductTypes(ref IDataContext context, ref bool dbChanges)
        {
            if (!context.ProductTypes.Any())
            {
                string path = $"{Directory.GetCurrentDirectory()}";
                var oReadData = File.ReadAllText($"{path}/Data/Seed/DataSeed/ProductTypeSeedData.json");
                var list = JsonConvert.DeserializeObject<List<ProductType>>(oReadData);
                foreach (var item in list)
                    context.ProductTypes.Add(item);

                dbChanges = true;
            }
        }
    }
}
