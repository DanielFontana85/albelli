﻿using Manufactoring.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Manufactoring.Data.Repository
{
    public class OrderRepository : IReadRepository<Order>, IWriteRepository<Order>
    {
        readonly IDataContext db;
        public OrderRepository(IDataContext _db)
        {
            this.db = _db;
        }

        public async Task<Order> Get(int id)
        {
            return await db.Orders.Include(o => o.Products)
                                  .ThenInclude(p => p.Type)
                                  .FirstOrDefaultAsync(x => x.OrderId == id);
        }

        public async Task<IEnumerable<Order>> GetAll(Expression<Func<Order,bool>> filter = null)
        {
            IQueryable<Order> query = db.Orders;

            if (filter != null)
                query = query.Where(filter);

            return await query.Include(o => o.Products)
                              .ThenInclude(p => p.Type)
                              .ToListAsync();
        }

        public async Task Add(Order obj)
        {
            await db.Orders.AddAsync(obj);

            await db.SaveChangesAsync();
        }

        public async Task<bool> Exists(int id)
        {
            return await db.Orders.AnyAsync(o => o.OrderId == id);
        }
    }
}
