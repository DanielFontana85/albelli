﻿using Manufactoring.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Manufactoring.Data.Repository
{
    public class ProductTypeRepository : IReadRepository<ProductType>
    {
        readonly IDataContext db;
        public ProductTypeRepository(IDataContext _db)
        {
            this.db = _db;
        }

        public async Task<bool> Exists(int id)
        {
            return await db.ProductTypes.AnyAsync(pt => pt.Id == id);
        }

        public async Task<ProductType> Get(int id)
        {
            return await db.ProductTypes.FindAsync(id);
        }

        public async Task<IEnumerable<ProductType>> GetAll(Expression<Func<ProductType, bool>> filter = null)
        {
            IQueryable<ProductType> query = db.ProductTypes;

            if (filter != null)
                query = query.Where(filter);

            return await db.ProductTypes.ToListAsync();
        }
    }
}
