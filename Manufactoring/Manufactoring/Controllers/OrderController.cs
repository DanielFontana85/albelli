﻿using AutoMapper;
using Manufactoring.DTOs;
using Manufactoring.Models;
using Manufactoring.Data;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using Manufactoring.Data.Repository;

namespace Manufactoring.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrderController : ControllerBase
    {
        readonly IOrderService services;
        readonly IReadRepository<Order> repo;
        readonly IMapper mapper;

        public OrderController(IOrderService _service,
                               IReadRepository<Order> _repo,
                               IMapper _mapper)
        {
            this.services = _service;
            this.repo = _repo;
            this.mapper = _mapper;
        }

        [HttpPost]
        public async Task<IActionResult> SubmitOrder(OrderDto input)
        {
            try
            {
                var errMsg = string.Empty;
                if (input == null || !input.IsValid(ref errMsg))
                    throw new ArgumentException($"Input is not valid. {errMsg}");

                var requiredBinWidth = await services.Add(mapper.Map<Order>(input));

                return Ok(requiredBinWidth);
            }
            catch (Exception ex)
            {
                return BadRequest($"There was an issue during the order's submition: '{ex.Message}'");
            }
        }

        [HttpGet("{orderID}")]
        public async Task<IActionResult> GetOrder(int orderID)
        {
            var order = await repo.Get(orderID);

            if (order == null)
                return BadRequest($"Order id: {orderID} was not found.");

            return Ok(mapper.Map<OutputOrderDto>(order));
        }
    }
}
