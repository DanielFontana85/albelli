﻿using Manufactoring.Data.Repository;
using Manufactoring.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Manufactoring
{
    public class ProductTypeService : IProductTypeService
    {
        readonly IReadRepository<ProductType> readProductTypeRepository;

        public ProductTypeService(IReadRepository<ProductType> _readProductTypeRepository)
        {
            this.readProductTypeRepository = _readProductTypeRepository;
        }

        public async Task<IEnumerable<Product>> SetProductTypes(Order order)
        {
            var productTypes = await readProductTypeRepository.GetAll();
            foreach (var item in order.Products)
            {
                item.Type = productTypes
                                   .Where(pt => pt.Name.ToLower().Trim() == item.Type.Name.ToLower().Trim())
                                   .FirstOrDefault();

                if (item.Type == null)
                    throw new ArgumentException($"Invalid product type: {item.Type.Name}");
            }

            return order.Products;
        }
    }
}
