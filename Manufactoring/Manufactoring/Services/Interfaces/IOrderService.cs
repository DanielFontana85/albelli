﻿using Manufactoring.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Manufactoring.Data
{
    public interface IOrderService
    {
        Task<decimal> Add(Order order);
    }
}
