﻿using Manufactoring.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Manufactoring
{
    public interface IProductTypeService
    {
        Task<IEnumerable<Product>> SetProductTypes(Order order);
    }
}
