﻿using Manufactoring.Data;
using Manufactoring.Data.Repository;
using Manufactoring.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Manufactoring
{
    public class OrderService : IOrderService
    {
        readonly IReadRepository<Order> readOrderRepository;
        readonly IWriteRepository<Order> writeOrderRepository;
        readonly IProductTypeService productTypeService;

        public OrderService(IReadRepository<Order> _readRepository, 
                            IWriteRepository<Order> _writeRepository,
                            IProductTypeService _productTypeService)
        {
            this.readOrderRepository = _readRepository;
            this.writeOrderRepository = _writeRepository;
            this.productTypeService = _productTypeService;
        }

        public async Task<decimal> Add(Order order)
        {
            if (await readOrderRepository.Exists(order.OrderId))
                throw new Exception($"Order Id: {order.OrderId} already exists");

            order.Products = await productTypeService.SetProductTypes(order);

            await writeOrderRepository.Add(order);

            return order.RequiredBinWidth;
        }

    }
}
