﻿using Manufactoring.Data.Repository;
using Manufactoring.Models;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Manufactoring.Tests
{
    public class ProductTypeServiceTest
    {
        [Fact]
        [Trait("Manufactoring", "OrderService")]
        public async Task SetProductTypesTest()
        {
            //Arrange 
            var expected = new List<Product>()
                {
                    new Product(){ Type= new ProductType(){ Name = "photoBook", Width = 19, NbrOfItemsCanStack = 1 }, Quantity = 1 },
                    new Product(){ Type= new ProductType(){ Name = "calendar", Width = 10, NbrOfItemsCanStack = 1 }, Quantity = 2 },
                    new Product(){ Type= new ProductType(){ Name = "mug", Width = 94, NbrOfItemsCanStack = 4 }, Quantity = 6 },
                };
            var order = new Order()
            {
                OrderId = 1006,
                Products = expected
            };

            var repo = new Mock<IReadRepository<ProductType>>();
            repo.Setup(m => m.GetAll(null)).ReturnsAsync(DummyData.GetFakeListOfProductType());
            var service = new ProductTypeService(repo.Object);

            //Act
            var actual = await service.SetProductTypes(order);

            //Assert
            Assert.True(expected.Equals(actual));
        }
    }
}
