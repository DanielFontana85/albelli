﻿using Manufactoring.Data.Repository;
using Manufactoring.Models;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Manufactoring.Tests
{
    public class OrderServiceTest
    {
        [Fact]
        [Trait("Manufactoring", "OrderService")]
        public async Task AddSuccessTest()
        {
            //Arrange
            var expected = 227;
            var order = new Order()
            {
                OrderId = 1006,
                Products = new List<Product>()
                {
                    new Product(){ Type= new ProductType(){ Name = "photoBook", Width = 19, NbrOfItemsCanStack = 1 }, Quantity = 1 },
                    new Product(){ Type= new ProductType(){ Name = "calendar", Width = 10, NbrOfItemsCanStack = 1 }, Quantity = 2 },
                    new Product(){ Type= new ProductType(){ Name = "mug", Width = 94, NbrOfItemsCanStack = 4 }, Quantity = 6 },
                }
            };

            var repo = new Mock<IWriteRepository<Order>>();
            var read = new Mock<IReadRepository<Order>>();
            read.Setup(m => m.Exists(order.OrderId)).ReturnsAsync(false);
            var ptService = new Mock<IProductTypeService>();
            ptService.Setup(m => m.SetProductTypes(order)).ReturnsAsync(DummyData.GetFakeProducts());
            var service = new OrderService(read.Object, repo.Object, ptService.Object);

            //Act
            var actual = await service.Add(order);

            //Assert
            Assert.Equal(actual, expected);
        }

        [Fact]
        [Trait("Manufactoring", "OrderService")]
        public async Task AddFailOrderAlreadyExistsTest()
        {
            //Arrange
            var order = new Order() { OrderId = 1002 };
            var expected = $"Order Id: {order.OrderId} already exists";

            var repo = new Mock<IWriteRepository<Order>>();
            var read = new Mock<IReadRepository<Order>>();
            read.Setup(m => m.Exists(order.OrderId)).ReturnsAsync(true);
            var ptService = new Mock<IProductTypeService>();
            var service = new OrderService(read.Object, repo.Object, ptService.Object);

            //Act
            Task act() => service.Add(order);

            //Assert
            var exception = await Assert.ThrowsAsync<Exception>(act);
            Assert.Equal(expected, exception.Message);
        }

        [Fact]
        [Trait("Manufactoring", "OrderService")]
        public async Task AddFailInvalidProductTest()
        {
            //Arrange
            var ptName = "otherThing";
            var order = new Order()
            {
                OrderId = 1003,
                Products = new List<Product>()
                {
                    new Product(){ Type= new ProductType(){ Name = ptName, Width = 19, NbrOfItemsCanStack = 1 }, Quantity = 1 }
                }
            };
            var expected = $"Invalid product type: {ptName}";

            var repo = new Mock<IWriteRepository<Order>>();
            var read = new Mock<IReadRepository<Order>>();
            read.Setup(m => m.Exists(order.OrderId)).ReturnsAsync(false);
            var ptService = new Mock<IProductTypeService>();
            ptService.Setup(m => m.SetProductTypes(order)).Throws(new ArgumentException(expected));
            var service = new OrderService(read.Object, repo.Object, ptService.Object);

            //Act
            Task act() => service.Add(order);

            //Assert
            var exception = await Assert.ThrowsAsync<ArgumentException>(act);
            Assert.Equal(expected, exception.Message);
        }
    }
}
