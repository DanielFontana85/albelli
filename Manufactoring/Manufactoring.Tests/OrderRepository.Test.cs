﻿using Manufactoring.Data;
using Manufactoring.Data.Repository;
using Manufactoring.Models;
using Microsoft.EntityFrameworkCore;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace Manufactoring.Tests
{
    public class OrderRepositoryTest : IDisposable
    {
        Order _order;
        IDataContext dbContext;
        public OrderRepositoryTest()
        {
            var options = new DbContextOptionsBuilder<DataContext>()
                            .UseInMemoryDatabase(databaseName: "Manufactoring")
                            .Options;

            dbContext = new DataContext(options);

            dbContext.ProductTypes.AddRange(DummyData.GetFakeListOfProductType());
            _order = DummyData.GetFakeOrder(dbContext.ProductTypes);
            dbContext.Orders.Add(_order);

            dbContext.SaveChangesAsync();
        }

        public void Dispose()
        {
            dbContext = null;
            _order = null;
        }

        #region Add

        [Fact]
        [Trait("Manufactoring", "OrderRepository")]
        public async Task AddSuccessTest()
        {
            //Arrange
            var order = new Order()
            {
                OrderId = 1008,
                Products = new List<Product>()
                {
                    new Product(){ Type= dbContext.ProductTypes.Local.FirstOrDefault(x=>x.Id==1), Quantity = 1 },
                    new Product(){ Type= dbContext.ProductTypes.Local.FirstOrDefault(x=>x.Id==2), Quantity = 2 },
                    new Product(){ Type= dbContext.ProductTypes.Local.FirstOrDefault(x=>x.Id==3), Quantity = 6 },
                }
            };

            var repo = new OrderRepository(dbContext);

            //Act
            await repo.Add(order);

            //Assert
            Assert.True(await dbContext.Orders.AnyAsync(o => o.OrderId == order.OrderId));
        }

        #endregion

        #region Get

        [Fact]
        [Trait("Manufactoring", "OrderRepository")]
        public async Task GetSuccessTest()
        {
            //Arrange
            var expected = _order;
            var repo = new OrderRepository(dbContext);

            //Act
            var actual = await repo.Get(_order.OrderId);

            //Assert
            Assert.True(actual.Equals(expected));
        }

        #endregion
    }
}
