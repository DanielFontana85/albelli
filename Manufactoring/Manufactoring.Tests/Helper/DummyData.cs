﻿using Manufactoring.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Manufactoring.Tests
{
    public static class DummyData
    {
        public static List<ProductType> GetFakeListOfProductType()
        {
            return new List<ProductType>() {
                        new ProductType() { Id = 1, Name = "photoBook", Width = 19, NbrOfItemsCanStack = 1 },
                        new ProductType() { Id = 2, Name = "calendar", Width = 10, NbrOfItemsCanStack = 1 },
                        new ProductType() { Id = 3, Name = "mug", Width = 94, NbrOfItemsCanStack = 4 }
                    };
        }

        public static Order GetFakeOrder(DbSet<ProductType> pts)
        {
            return new Order()
            {
                OrderId = 1002,
                Products = new List<Product>()
                            {
                                new Product(){ Type = pts.Local.FirstOrDefault(x=>x.Id==1), Quantity = 1 },
                                new Product(){ Type = pts.Local.FirstOrDefault(x=>x.Id==2), Quantity = 2 },
                                new Product(){ Type = pts.Local.FirstOrDefault(x=>x.Id==3), Quantity = 6 },
                            }
            };
        }

        public static IEnumerable<Product> GetFakeProducts()
        {
            return new List<Product>()
                {
                    new Product(){ Type= new ProductType(){ Id=1, Name = "photoBook", Width = 19, NbrOfItemsCanStack = 1 }, Quantity = 1 },
                    new Product(){ Type= new ProductType(){ Id=2, Name = "calendar", Width = 10, NbrOfItemsCanStack = 1 }, Quantity = 2 },
                    new Product(){ Type= new ProductType(){ Id=3, Name = "mug", Width = 94, NbrOfItemsCanStack = 4 }, Quantity = 6 },
                };
        }
    }
}
