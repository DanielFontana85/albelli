using AutoMapper;
using Manufactoring.Controllers;
using Manufactoring.Data;
using Manufactoring.Data.Repository;
using Manufactoring.DTOs;
using Manufactoring.Models;
using Microsoft.AspNetCore.Mvc;
using Moq;
using System;
using System.Collections.Generic;
using Xunit;

namespace Manufactoring.Tests
{
    public class OrderControllerTest
    {
        #region SubmitOrder

        [Fact]
        [Trait("Manufactoring", "OrderController")]
        public void SubmitOrderSuccessTest()
        {
            //Arrange
            decimal expected = 227;
            var input = new OrderDto()
            {
                OrderID = 1001,
                Products = new List<ProductDto>()
                {
                    new ProductDto(){ ProductType = "photoBook", Quantity = 1 },
                    new ProductDto(){ ProductType = "calendar", Quantity = 2 },
                    new ProductDto(){ ProductType = "mug", Quantity = 6 },
                }
            };
            var order = new Order()
            {
                OrderId = input.OrderID,
                Products = new List<Product>()
                {
                    new Product(){ Type= new ProductType(){ Name = "photoBook", Width = 19, NbrOfItemsCanStack = 1 }, Quantity = 1 },
                    new Product(){ Type= new ProductType(){ Name = "calendar", Width = 10, NbrOfItemsCanStack = 1 }, Quantity = 2 },
                    new Product(){ Type= new ProductType(){ Name = "mug", Width = 94, NbrOfItemsCanStack = 4 }, Quantity = 6 },
                }
            };

            var service = new Mock<IOrderService>();
            service.Setup(m => m.Add(order)).ReturnsAsync(expected);
            var repo = new Mock<IReadRepository<Order>>();
            var mapper = new Mock<IMapper>();
            mapper.Setup(m => m.Map<Order>(input)).Returns(order);

            var orderController = new OrderController(service.Object, repo.Object, mapper.Object);

            //Act
            var result = (ObjectResult)orderController.SubmitOrder(input).Result;
            var actual = (decimal)result.Value;

            //Assert
            Assert.Equal(actual, expected);
        }

        [Theory]
        [InlineData(0)]
        [InlineData(1001)]
        [Trait("Manufactoring", "OrderController")]
        public void SubmitOrderInvalidOrderIdAndProductsTest(int orderID)
        {
            //Arrange
            var errMsg = $"Input is not valid. {(orderID > 0 ? "Products are mandatory." : "Order id is lower than 0.")}";
            var expected = $"There was an issue during the order's submition: '{errMsg}'";
            var input = new OrderDto() { OrderID = orderID };

            var mapper = new Mock<IMapper>();
            var repo = new Mock<IReadRepository<Order>>();
            var service = new Mock<IOrderService>();
            var orderController = new OrderController(service.Object, repo.Object, mapper.Object);

            //Act
            var result = (ObjectResult)orderController.SubmitOrder(input).Result;
            var actual = (string)result.Value;

            //Assert
            Assert.Equal(actual, expected);
        }

        [Fact]
        [Trait("Manufactoring", "OrderController")]
        public void SubmitOrderInvalidProductQuantityTest()
        {
            var errMsg = "Input is not valid. One or more product's quantities are lower than 0.";
            var expected = $"There was an issue during the order's submition: '{errMsg}'";
            var input = new OrderDto()
            {
                OrderID = 1001,
                Products = new List<ProductDto>() { new ProductDto() { ProductType = "photoBook", Quantity = 0 } }
            };

            var mapper = new Mock<IMapper>();
            var repo = new Mock<IReadRepository<Order>>();
            var service = new Mock<IOrderService>();
            var orderController = new OrderController(service.Object, repo.Object, mapper.Object);

            //Act
            var result = (ObjectResult)orderController.SubmitOrder(input).Result;
            var actual = (string)result.Value;

            //Assert
            Assert.Equal(actual, expected);
        }

        #endregion

        #region GetOrder

        [Fact]
        [Trait("Manufactoring", "OrderController")]
        public void GetOrderSuccessTest()
        {
            //Arrange
            var orderId = 1001;
            var order = new Order()
            {
                OrderId = orderId,
                Products = new List<Product>()
                {
                    new Product(){ Type= new ProductType(){ Name="photoBook", Width=19, NbrOfItemsCanStack=1 }, Quantity=1 },
                    new Product(){ Type= new ProductType(){ Name="calendar", Width=10, NbrOfItemsCanStack=1 }, Quantity=2 },
                    new Product(){ Type= new ProductType(){ Name="mug", Width=94, NbrOfItemsCanStack=4 }, Quantity=6 },
                }
            };
            var expected = new OutputOrderDto()
            {
                OrderID = orderId,
                Products = new List<ProductDto>()
                {
                    new ProductDto(){ ProductType="photoBook", Quantity=1 },
                    new ProductDto(){ ProductType="calendar", Quantity=2 },
                    new ProductDto(){ ProductType="mug", Quantity=6 },
                },
                RequiredBinWidth = 227
            };

            var service = new Mock<IOrderService>();
            var repo = new Mock<IReadRepository<Order>>();
            repo.Setup(m => m.Get(orderId)).ReturnsAsync(order);
            var mapper = new Mock<IMapper>();
            mapper.Setup(m => m.Map<OutputOrderDto>(order)).Returns(expected);

            var orderController = new OrderController(service.Object, repo.Object, mapper.Object);

            //Act
            var result = (ObjectResult)orderController.GetOrder(orderId).Result;
            var actual = (OutputOrderDto)result.Value;

            //Assert
            Assert.True(actual.Equals(expected));
        }

        [Fact]
        [Trait("Manufactoring", "OrderController")]
        public void GetOrderFailTest()
        {
            //Arrange
            var orderId = 1001;
            var expected = $"Order id: {orderId} was not found.";
            Order order = null;

            var service = new Mock<IOrderService>();
            var repo = new Mock<IReadRepository<Order>>();
            repo.Setup(m => m.Get(orderId)).ReturnsAsync(order);
            var mapper = new Mock<IMapper>();

            var orderController = new OrderController(service.Object, repo.Object, mapper.Object);

            //Act
            var result = (ObjectResult)orderController.GetOrder(orderId).Result;
            var actual = (string)result.Value;

            //Assert
            Assert.Equal(actual, expected);
        }

        #endregion
    }
}
